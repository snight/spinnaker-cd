# Spinnaker部署指南
# 一、部署spinnaker第一部分
## 软件版本：
- Kubernetes 1.20.0 集群
- Docker CE 20.10.1
- Docker-Compose：1.18
- Harbor：2.1.1
- Minio：latest （可选）
- MySQL :5.7
- Redis：6.0.9
- kubectl：1.20.0
- halyard：1.40.1-20201106154048
- Spinnaker 1.23.4 组件版本
  - Deck：3.4.0-20201014120142
  - Gate：1.19.0-20201012200017
  - Clouddriver：7.0.3-20201201114456
  - Front50：0.26.0-20201013163321
  - Igor：1.13.0-20201009170017
  - kayenta：0.18.0-20201009200017
  - Orca：2.17.1-20201208101954
  - Rosco：0.22.0-20201009170017
  - Fiat：1.14.0-20201009170017
  - Echo：2.15.0-20201009170017
  - Monitoring-Daemon：0.19.0-20201013140017
  - Monitoring-Third-Party：0.19.0-20201013140017 （IMAGE ID 同上）
## 实验环境
应用服务|主机名|IP地址|端口|用途|部署方式|账号 / 密码
-------|:---:|:---|:---|:---|:----:|:-----
Harbor|harbor|10.10.51.193|80,443|镜像仓库|Docker-Compose|admin / Harbor12345
Jenkins|jenkins|10.10.51.194|8080|Jenkins|Docker-Compse|admin / 123456
Mysql|db-server|10.10.51.195|3306|Mysql数据库|Docker-Compose|root / 123456
Redis|db-server|10.10.51.195|6379|Redis数据库|Docker-Compose|/
Minio|minio|10.10.51.196|9000|Minio对象存储|Docker|admin / password
Halyard|spinnaker|10.10.51.199|8084,9000|Spinnaker部署工具|Docker|/
Kubernetes|k8s-cluster|10.10.51.200|6443|K8S集群API|K8S|/
## 数据持久化
- Orca 、Clouddriver 默认安装使用redis存储数据，转换为使用SQL数据库存储。
- Front50 默认安装使用s3持久化存储，转换为使用SQL数据库存储。
- 使用外部redis服务。
- mysql和redis服务的data目录映射到本地/opt/spinnaker的目录下。

## 1、准备工作
在spinnaker主机上创建工作目录
```shell
# 下载spinnaker部署脚本
git clone https://gitlab.com/snight/spinnaker-cd.git

# 创建redis/mysql数据库目录
mkdir -p /opt/spinnaker/{redis,mysql}

# 在root用户下创建.kube目录,用于存放k8s-config文件
mkdir -p ~/.kube

# 在root用户下创建.hal目录,用于存放bom文件夹
mkdir -p ~/.hal

# 安装常用工具
yum install -y wget zip unzip

# 下载代码
```
准备k8s-config文件<br>
由于安装spinnaker部署工具的主机并不是K8S集群成员，所以需要从K8S集群上的任一master节点将config文件复制到spinnaker主机，使其可以执行kubectl命令，为后面做spinnaker和kubernetes集成做准备。
```shell
## 在k8s集群的任一master节点上运行
# 将master节点的config文件复制到运行halyard容器的目标主机
scp ~/.kube/config root@spinnaker:~/.kube/
```
测试在spinnaker主机上是否可以获取k8s集群节点信息
```shell
## 在spinnaker主机上运行
# 安装kubectl
yum install -y kubectl:1.20.0
# 获取k8s集群节点信息
kube get nodes
# 创建spinnaker命名空间
kubectl create ns spinnaker
```
## 2、部署MinIO（可选）
**Front50 组件默认使用S3持久化存储，如果使用SQL数据库存储，则直接跳过本节内容。<br>**
**假设以前已经创建过一个名称为`rook-block`的存储类。<br>**
```shell
cd spinnaker-cd/minio
kubectl create -f minio-deployment.yaml
kubectl create -f minio-service.yaml
kubectl create -f minio-job.yaml
```
### 3、部署Redis
准备redis镜像
```
docker pull redis:6.0.9
```
启动redis服务
```shell
cd spinnaker-cd/redis
docker-compose up -d
```
### 4、部署mysql
准备镜像
```
docker pull mysql:5.7
```
启动mysql服务
```bash
cd /opt/spinnaker-cd/mysql
docker-compose up -d
```
# 二、部署spinnaker第二部分
## 1、进入mysql的容器
```shell
docker exec -u root -it mysql bash
mysql -uroot -p123456
```
## 2、创建Clouddriver数据库
```sql
CREATE DATABASE clouddriver DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
GRANT
   SELECT, INSERT, UPDATE, DELETE, CREATE, EXECUTE, SHOW VIEW
 ON clouddriver.*
 TO 'clouddriver_service'@'%' IDENTIFIED BY 'clouddriver@spinnaker.com';
GRANT
   SELECT, INSERT, UPDATE, DELETE, CREATE, DROP, REFERENCES, INDEX, ALTER, LOCK TABLES, EXECUTE, SHOW VIEW
 ON clouddriver.*
 TO 'clouddriver_migrate'@'%' IDENTIFIED BY 'clouddriver@spinnaker.com';
```
## 3、创建Front50数据库
```sql
CREATE DATABASE front50 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci; 
GRANT SELECT, INSERT, UPDATE, DELETE, CREATE, EXECUTE, SHOW VIEW ON front50.*  TO 'front50_service'@'%' IDENTIFIED BY "front50@spinnaker.com"; 
GRANT SELECT, INSERT, UPDATE, DELETE, CREATE, DROP, REFERENCES, INDEX, ALTER, LOCK TABLES, EXECUTE, SHOW VIEW ON front50.* TO 'front50_migrate'@'%' IDENTIFIED BY "front50@spinnaker.com"; 
```
## 4、创建Orca数据库
```sql
set tx_isolation = 'REPEATABLE-READ';
CREATE SCHEMA orca DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
GRANT 
 SELECT, INSERT, UPDATE, DELETE, CREATE, EXECUTE, SHOW VIEW
 ON orca.* 
 TO 'orca_service'@'%' IDENTIFIED BY "orca@spinnaker.com" ;
GRANT 
 SELECT, INSERT, UPDATE, DELETE, CREATE, DROP, REFERENCES, INDEX, ALTER, LOCK TABLES, EXECUTE, SHOW VIEW 
 ON orca.* 
 TO 'orca_migrate'@'%'  IDENTIFIED BY "orca@spinnaker.com" ;
```
## 5、退出mysql容器
```sql
mysql> quit
exit
```
# 三、自定义Halyard配置与服务
## 1、创建自定义配置和服务的目录
- 根据实际环境，修改下面的命令，将文件内的`<MYSQL_SERVER_IP>`和`<REDIS_SERVER_IP>`替换成数据库的IP地址。
- 如果需要修改默认端口，直接修改YML文件内容即可。
```shell
sed -i "s|<MYSQL_SERVER_IP>|10.10.51.195|" spinnaker-cd/halyard-bom-install/default/profiles/clouddriver-local.yml
sed -i "s|<MYSQL_SERVER_IP>|10.10.51.195|" spinnaker-cd/halyard-bom-install/default/profiles/front50-local.yml
sed -i "s|<MYSQL_SERVER_IP>|10.10.51.195|" spinnaker-cd/halyard-bom-install/default/profiles/orca-local.yml
sed -i "s|<REDIS_SERVER_IP>|10.10.51.195|" spinnaker-cd/halyard-bom-install/default/service-settings/redis.yml
```
## 2、修改权限
```shell
chown -R 1000:1000 spinnaker-cd/halyard-bom-install/default
chown -R 777 spinnaker-cd/halyard-bom-install/default
```
# 四、部署Spinnaker第四部分
## 1、准备 .bom 文件夹
```shell
cd spinnaker-cd/halyard-bom-install
chmod 777 -R .bom
cp .bom /root/.hal/
```
## 2、启动 Halyard 容器
**如果K8S主机使用了域名的方式，还需要添加`--add-host`参数为容器添加hosts主机解析，避免容器内出现找不到K8S主机的问题。**
```shell
docker run -d -p8084:8084 -p9000:9000 \
--name halyard \
--add-host k8s-cluster:10.10.51.200 \
-v /root/.hal:/home/spinnaker/.hal \
-v /root/.kube:/home/spinnaker/.kube \
-it registry.cn-qingdao.aliyuncs.com/k8s-devops/halyard:1.40.1-20201106154048
```
## 3、复制相关配置至halyard容器
```shell
cd spinnaker-cd/halyard-bom-install
docker cp halyard.yml halyard:/opt/halyard/config/halyard-local.yml
docker cp default halyard:/home/spinnaker/.hal/
docker stop halyard  &&  docker start halyard
```
# 五、部署Spinnaker第五部分
## 1、Halyard初始化配置
```shell
# 设置Spinnaker版本，--version 指定版本
hal config version edit --version local:1.23.4 --no-validate

# 设置时区
hal config edit --timezone Asia/Shanghai

# Storage 配置基于minio搭建的S3存储 (可选,使用时取消注释,填写<>内的参数)
# hal config storage s3 edit \
#     --endpoint <MINIO_ENDPOINT> \
#     --access-key-id <MINIO_ACCESS_KEY> \
#     --secret-access-key <MINIO_SECRET_KEY> \
#     --bucket spinnaker \
#     --path-style-access true --no-validate

# 设置s3存储,不管后期用与不用，都需要设置
hal config storage edit --type s3 --no-validate
```
## 2、添加镜像仓库与K8S集群
```shell
# 开启Docker Registry镜像仓库
hal config provider docker-registry enable --no-validate
# 设置镜像仓库的名称、地址、账号和密码,这里以harbor为例
hal config provider docker-registry account add harbor \
    --address <HARBOR-SERVER-IP> \
    --insecure-registry true \
    --username admin \
    --password Harbor12345

# 添加kubernetes provider账户
hal config provider kubernetes enable --no-validate
hal config provider kubernetes account add k8s-v2-account \
    --docker-registries harbor \
    --context $(kubectl config current-context) \
    --service-account true \
    --omit-namespaces=kube-system,kube-public \
    --provider-version v2 \
    --no-validate

# 编辑Spinnaker部署选项，分布式部署，命名空间。
hal config deploy edit \
    --account-name k8s-v2-account \
    --type distributed \
    --location spinnaker
```
## 3、开启特性功能
```shell
# 开启一些主要的功能
hal config features edit --pipeline-templates true
hal config features edit --artifacts true
hal config features edit --managed-pipeline-templates-v2-ui true
```
## 4、配置Jenkins CI集成
```shell
# 配置Jenkins
hal config ci jenkins enable
# 填写Jenkins的账号和密码
hal config ci jenkins master add jenkins-master \
    --address <JENKINS_ADDRESS> \
    --username <JENKINS_USERNAME> \
    --password <JENKINS_PASSWORD>
# 启用csrf
hal config ci jenkins master edit jenkins-master --csrf true
```
## 5、配置GitHub/GitLab集成
```shell
## GitHub
## 参考配置文档:https://spinnaker.io/setup/artifacts/github/
# 创建token https://github.com/settings/tokens
# hal config artifact github enable
# hal config artifact github account add  \
# --token   \
# --username 

## GitLab
## 参考配置文档:https://spinnaker.io/setup/artifacts/gitlab/
# 创建一个root账号的token（root）
hal config artifact gitlab enable
hal config artifact gitlab account add root \
    --token TndyZn7QAJjzQhm8DE5T
```
## 6、设置deck与gate的域名
**这里为了访问方便，不使用域名域名访问方式，而采用集群节点IP + NodePort的方式，故做以下配置：**
```shell
hal config security ui edit --override-base-url / --no-validate
hal config security api edit --override-base-url /gate --no-validate
```
## 7、发布
开始部署Spinnaker
```
hal deploy apply --no-validate
```
查看服务
```
kubectl get pod,svc -n spinnaker
```
## 8、创建spinnaker服务的资源清单
```shell
kubectl apply -f spinnaker-cd/halyard-bom-install/spinnaker-rbac.yaml
kubectl apply -f spinnaker-cd/halyard-bom-install/spinnaker-clusterrolebinding.yaml.yaml
kubectl apply -f spinnaker-cd/halyard-bom-install/spinnaker-service.yaml
```
## 9、访问Spinnaker
访问 `http://<任意K8S集群节点的IP>:30009`

## 10、清理删除Spinnaker
如果部署过程失败，或者需要重新部署，需要执行：
```shell
# 清理spinnaker
docker exec -it halyard hal deploy clean

# 删除.hal相关目录
rm -rf ${BOMS_DIR}/config ${BOMS_DIR}/default ${BOMS_DIR}/.boms

# 删除halyard容器
docker stop halyard && docker rm halyard
```
