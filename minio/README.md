# 部署Minio

# Docker-Compose本地部署
启动Minio
```
cd spinnaker-cd/minio
docker-compose up -d
```
停止Minio
```
cd spinnaker-cd/minio
docker-compose down
```

# 部署到K8S
假设已有名称为ceph-block的存储类
```
kubectl apply spinnaker-cd/minio/minio-deployment.yaml
kubectl apply spinnaker-cd/minio/minio-service.yaml
kubectl apply spinnaker-cd/minio/minio-job.yaml
```
