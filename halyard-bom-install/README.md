
# halyard.sh
Spinnaker的配置脚本 
```bash
# 指定Spinnaker版本
VERSION="1.23.4"
# Minio的地址
MINIO_ENDPOINT=http://minio:9000
# Minio的用户名
MINIO_ACCESS_KEY=admin
# Minio的密钥
MINIO_SECRET_KEY=password
# 镜像仓库的地址
REGISTRY_ADDRESS=10.10.51.193
# JENKINS的服务器地址
JENKINS_ADDRESS=10.10.51.194
# JENKINS的账号
JENKINS_USERNAME=admin
# JENKINS的密码
JENKINS_PASSWORD=123456
```

# halyard.yml
禁用Google Cloud Storage的配置文件

# install.sh
自动化部署Spinnaker的脚本 
```bash
# 指定Spinnaker版本
VERSION="1.23.4"
# BOMS目录
BOMS_DIR="/root/.hal/"
# BOMS文件夹名
BOMS_FILR=".boms"
# Kube目录
KUBE_DIR="/root/.kube/"
# Halyard镜像
HAL_IMAGE="registry.cn-qingdao.aliyuncs.com/k8s-devops/halyard:1.40.1-20201106154048"
```
# install.sh的参数
```bash
# 执行安装脚本
./install install
# 执行安装清理
./install clean
# 执行完清理然后安装
./install all
```
