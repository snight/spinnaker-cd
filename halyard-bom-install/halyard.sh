#!/bin/bash

VERSION="1.23.4"
MINIO_ENDPOINT=http://minio:9000
MINIO_ACCESS_KEY=admin
MINIO_SECRET_KEY=password
REGISTRY_ADDRESS=10.10.51.193
JENKINS_ADDRESS=10.10.51.194
JENKINS_USERNAME=admin
JENKINS_PASSWORD=123456
until hal --ready; do sleep 10 ; done

# 设置Spinnaker版本，--version 指定版本
hal config version edit --version local:${VERSION} --no-validate

## 设置时区
hal config edit --timezone Asia/Shanghai

## Storage 配置基于minio搭建的S3存储 (可选)
# hal config storage s3 edit \
#     --endpoint $MINIO_ENDPOINT \
#     --access-key-id $MINIO_ACCESS_KEY \
#     --secret-access-key $MINIO_SECRET_KEY \
#     --bucket spinnaker \
#     --path-style-access true --no-validate
hal config storage edit --type s3 --no-validate

# 开启docker-registry镜像仓库
hal config provider docker-registry enable --no-validate
hal config provider docker-registry account add harbor \
    --address $REGISTRY_ADDRESS \
    --insecure-registry true \
    --username admin \
    --password Harbor12345


# 添加kubernetes provider账户
hal config provider kubernetes enable --no-validate
hal config provider kubernetes account add k8s-v2-account \
    --docker-registries harbor \
    --context $(kubectl config current-context) \
    --service-account true \
    --omit-namespaces=kube-system,kube-public \
    --provider-version v2 \
    --no-validate

## 编辑Spinnaker部署选项，分布式部署，命名空间。
hal config deploy edit \
    --account-name k8s-v2-account \
    --type distributed \
    --location spinnaker

## 开启一些主要的功能
hal config features edit --pipeline-templates true
# hal config features edit --artifacts true
# hal config features edit --managed-pipeline-templates-v2-ui true

# 配置Jenkins
hal config ci jenkins enable
### JenkinsServer 需要用到账号和密码
hal config ci jenkins master add jenkins-master \
    --address $JENKINS_ADDRESS \
    --username $JENKINS_USERNAME \
    --password $JENKINS_PASSWORD
### 启用csrf
hal config ci jenkins master edit jenkins-master --csrf true


# GitHub
## 参考：https://spinnaker.io/setup/artifacts/github/
## 创建token https://github.com/settings/tokens
# hal config artifact github enable
# hal config artifact github account add <github-account> \
#     --token <token>  \
#     --username <username>
 
# GitLab
## https://spinnaker.io/setup/artifacts/gitlab/
## 创建一个个人的token（admin）
hal config artifact gitlab enable
hal config artifact gitlab account add root \
    --token TndyZn7QAJjzQhm8DE5T


## 设置deck与gate的域名
hal config security ui edit --override-base-url / --no-validate
hal config security api edit --override-base-url /gate --no-validate

##发布
hal deploy apply --no-validate
