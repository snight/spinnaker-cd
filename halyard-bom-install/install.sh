#!/bin/bash
VERSION="1.23.4"
BOMS_DIR="/root/.hal/"
BOMS_FILR=".boms"
KUBE_DIR="/root/.kube/"
HAL_IMAGE="registry.cn-qingdao.aliyuncs.com/k8s-devops/halyard:1.40.1-20201106154048"

function Clean(){
    echo -e "\033[43;34m =====Clean===== \033[0m"
    rm -rf ${BOMS_DIR}/config ${BOMS_DIR}/default ${BOMS_DIR}/.boms
    docker stop halyard && docker rm halyard
}

## 安装
function Install(){
    echo -e "\033[43;34m =====Install===== \033[0m"
    [ -d ${BOMS_DIR} ] || mkdir ${BOMS_DIR} 
    cp -rf ${BOMS_FILR} ${BOMS_DIR}
    ls -a ${BOMS_DIR}
    chmod 777 -R ${BOMS_DIR}
    chmod 777 -R ${KUBE_DIR}
    chown 1000:1000 -R default
    
    docker run -d -p8084:8084 -p9000:9000 \
    --name halyard \
    --add-host k8s-cluster:10.10.51.200 \
    -v ${BOMS_DIR}:/home/spinnaker/.hal \
    -v ${KUBE_DIR}:/home/spinnaker/.kube \
    -it ${HAL_IMAGE}
    sleep 5
    docker cp halyard.yml halyard:/opt/halyard/config/halyard-local.yml
    sleep 3
    docker cp default halyard:/home/spinnaker/.hal/
    sleep 3
    docker stop halyard  &&  docker start halyard
    sleep 3
    docker ps | grep halyard
    sleep 3
    chmod +x halyard.sh
    docker cp halyard.sh halyard:/home/spinnaker/halyard.sh
    sleep 5
    docker exec -it halyard ./home/spinnaker/halyard.sh
    sleep 10
    kubectl get pod,svc -n spinnaker
}


case $1 in 
  clean)
    Clean
    ;;
  install)
    Install
    ;;
  all)
    Clean
    Install
    ;;
    
  *)
    echo -e " [Clean -> Install = all] "
    ;;
esac
